package pv.model;

/**
 * @author jianzou
 *
 */
public class DemoData {
	private String id = null;
	private String type = null;
	private String value = null;
	
	public DemoData(String id, String type, String value) {
		super();
		this.id = id;
		this.type = type;
		this.value = value;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
