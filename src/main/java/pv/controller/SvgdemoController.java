package pv.controller;

import java.util.Random;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pv.model.DemoData;

@Controller
public class SvgdemoController {
	
    @RequestMapping("/svgdemo")
    public String svgdemo(@RequestParam(value="svgfileid", required=false, defaultValue="1") String svgFileId, Model model) {
        model.addAttribute("name", svgFileId);
        return "svgdemo";
    }
    
    @RequestMapping("/getdemodata")
    public ResponseEntity<DemoData> getDemoData() {
    	Random random = new Random();
        int stubData = random.nextInt(100);
        DemoData stubdata = new DemoData("demotext", "text", ""+stubData);
        return new ResponseEntity<DemoData>(stubdata, HttpStatus.OK);
    }
    
}
